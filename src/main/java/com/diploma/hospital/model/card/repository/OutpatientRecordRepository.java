package com.diploma.hospital.model.card.repository;

import com.diploma.hospital.model.card.OutpatientRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OutpatientRecordRepository extends JpaRepository<OutpatientRecord, UUID> {
}
