package com.diploma.hospital.model.patient.document.repository;

import com.diploma.hospital.model.patient.document.DocumentType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DocumentTypeRepository extends JpaRepository<DocumentType, UUID> {
}
