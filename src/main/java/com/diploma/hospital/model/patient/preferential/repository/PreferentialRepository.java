package com.diploma.hospital.model.patient.preferential.repository;

import com.diploma.hospital.model.patient.preferential.Preferential;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PreferentialRepository extends JpaRepository<Preferential, UUID> {
}
